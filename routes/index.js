var express = require('express');
var router = express.Router();
require('dotenv').config();
var utilController = require('../controller/utils');
const appId = process.env.appId;
const apiUrl = 'https://openexchangerates.org/api/latest.json';

router.get('/',utilController.getRates);
router.post('/',utilController.generateValue);


module.exports = router;
