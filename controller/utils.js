const request = require('request');
const appId = process.env.appId;
const apiUrl = 'https://openexchangerates.org/api/latest.json';
const baseCurrency = 'USD';
const jsonQuery = require('json-query');
require('dotenv').config();

exports.getRates = function(req,res,next){
    var queryData=[];

    var query ={
        app_id:appId,
        base:baseCurrency
    };
   const optionVariables= {
       url:apiUrl,
       qs:query,
       timeout:5000
   };
   request(optionVariables,function (error,response,body) {
      if(!error){
           const data = JSON.parse(body);
           console.log(data.rates['KES']);
           res.render('user/converter',{currentUsd:data.rates['KES'] , isExecuted:false});
      } else{
          queryData.push('Error Processing Request');
          console.log('Error Processing Request');
      }
   });
};
exports.generateValue = function(req,res,next){
    const currentConversion = req.body.currentConversion;
    const amountInUsd = req.body.USD;
    const generatedAmountInShilling = currentConversion * amountInUsd;
    console.log(generatedAmountInShilling);
    res.render('user/converter',{generatedAmount:generatedAmountInShilling,isExecuted:true});
};
